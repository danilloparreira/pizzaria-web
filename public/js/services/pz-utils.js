angular.module('pzUtils', ['ngMaterial'])
        .factory('pzMessages', function ($mdToast) {
            var serviceInstance = {
                messageShow: messageShow
            };
            function messageShow(text, afterTime) {
                if (typeof (afterTime) == "undefined") {
                    $mdToast.show(
                            $mdToast.simple()
                            .textContent(text)
                            .position('top right')
                            .hideDelay(5000));
                } else {
                    setTimeout(function () {
                        $mdToast.show(
                                $mdToast.simple()
                                .textContent(text)
                                .position('top right')
                                .hideDelay(5000));
                    }, afterTime);
                }
            }
            return serviceInstance;
        });