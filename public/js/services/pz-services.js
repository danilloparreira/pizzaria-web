angular.module('pzServices', ['ngResource'])
        .factory('resourcesState', function ($resource) {

            return $resource(uriRest + '/state/:stateId', null, {
                'update': {
                    method: 'PUT'
                }
            });
        })
        .factory("registerState", function (resourcesState, $q, $rootScope) {
            var service = {};
            service.register = function (state) {
                return $q(function (resolve, reject) {

                    if (state.id) {
                        resourcesState.update({stateId: state.id}, state, function () {
                            $rootScope.$broadcast('update');
                            resolve({
                                alertMessage: 'State ' + state.name + ' atualizado com sucesso',
                                inclusion: false
                            });
                        }, function (erro) {
                            console.log(erro);
                            reject({
                                alertMessage: 'Não foi possível atualizar o State ' + state.name
                            });
                        });
                    } else {
                        resourcesState.save(state, function () {
                            resolve({
                                alertMessage: 'State ' + state.name + ' incluído com sucesso',
                                inclusion: true
                            });
                            $rootScope.$broadcast('saved');
                        }, function (erro) {
                            console.log(erro);
                            reject({
                                alertMessage: 'Não foi possível incluir a State ' + state.name
                            });
                        });
                    }
                });
            };
            return service;
        });