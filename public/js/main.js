var uriRest = 'http://localhost:8080/pizzaria';

function deleteDate(entity) {
    delete entity.dateUpdate;
    delete entity.dateRegister;
}

(function () {
    'use strict';
    var pizzariaAPP = angular.module('pizzariaAPP', [
        'ngRoute', 'ngAria', 'ngMaterial', 'ngMessages', 'material.svgAssetsCache',
        'pzServices', 'pzDirectives', 'ngMaterialSidemenu', 'pzUtils'

    ]);
    pizzariaAPP.config(['$routeProvider', function ($routeProvider) {

            crudRouter('state', $routeProvider);
            crudRouter('city', $routeProvider);
            crudRouter('district', $routeProvider);

            $routeProvider.when('/register', {
                templateUrl: 'partials/global/register.html'
            }).when('/index', {
                templateUrl: 'partials/global/home.html'
            }).otherwise({redirectTo: '/index'});
        }])

            .controller('AppCtrl', ['$scope', '$mdSidenav', function ($scope, $mdSidenav) {
                    $scope.showMobileMainHeader = true;
                    $scope.toggleSideNavPanel = function () {
                        $mdSidenav('left').toggle();
                    };
                }]);
            
    function crudRouter(name, routeProvider) {
        routeProvider.when('/' + name, {
            templateUrl: 'partials/' + name + '/list.html',
            controller: name.toAllCamelCase() + 'sController'
        }).when('/' + name + '/new', {
            templateUrl: 'partials/' + name + '/form.html',
            controller: name.toAllCamelCase() + 'Controller'
        }).when('/' + name + '/edit/:id', {
            templateUrl: 'partials/' + name + '/form.html',
            controller: name.toAllCamelCase() + 'Controller'
        });
    }

    String.prototype.toCamelCase = function () {
        return this.replace(/[^A-Za-z0-9]/g, ' ').replace(/^\w|[A-Z]|\b\w|\s+/g,
                function (match, index) {
                    if (+match === 0 || match === '-' || match === '.')
                        return "";
                    return index === 0 ? match.toLowerCase() :
                            match.toUpperCase();
                });
    };
    String.prototype.toAllCamelCase = function () {
        return this.toCamelCase().charAt(0).toUpperCase() +
                this.toCamelCase().slice(1);
    };

})();
