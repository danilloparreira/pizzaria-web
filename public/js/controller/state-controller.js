/* global uriRest */

angular.module('pizzariaAPP').controller('StateController', function ($scope, $http, $routeParams, pzMessages) {
    $scope.state = {};
    
    if ($routeParams.id) {
        $http.get(uriRest + '/state/' + $routeParams.id).success(function (state) {
            $scope.state = state;
        }).error(function (erro) {
            console.log(erro);
            pzMessages.messageShow("Não foi possível obter o estado");
        });
    }

    $scope.submit = function () {
        if ($scope.form.$valid) {
            if ($scope.state.id) {
                deleteDate($scope.state);
                $http.put(uriRest + '/state/' + $scope.state.id, $scope.state).success(function (state) {
                    pzMessages.messageShow("O estado " + $scope.state.name + " atualizado com sucesso.");
                    $scope.state = state;
                }).error(function (erro) {
                    console.log(erro);
                    console.log($scope.state);
                    
                    pzMessages.messageShow('Não foi possível alterar o estado ' + $scope.state.name);
                });
            } else {
                $http.post(uriRest + "/state", $scope.state).success(function () {
                    $scope.state = {};
                    pzMessages.messageShow("Estado adicionado com sucesso");
                }).error(function (erro) {
                    console.log(erro);
                    pzMessages.messageShow("Não foi possível adicionar o estado");
                });
            }
        }
    };
});