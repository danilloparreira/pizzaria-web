/* global uriRest */

angular.module('pizzariaAPP').controller('CityController', function ($scope, $http, $routeParams, pzMessages) {
    $scope.city = {};

    if ($routeParams.id) {
        $http.get(uriRest + '/city/' + $routeParams.id).success(function (city) {
            $scope.city = city;
            console.log($scope.city);
        }).error(function (erro) {
            console.log(erro);
            pzMessages.messageShow("Não foi possível obter a cidade");
        });
    }

    $scope.submit = function () {
        if ($scope.form.$valid) {
            if ($scope.city.id) {
                deleteDate($scope.city);
                deleteDate($scope.city.state);
                $http.put(uriRest + '/city/' + $scope.city.id, $scope.city).success(function (city) {
                    pzMessages.messageShow("A cidade " + $scope.city.name + " atualizado com sucesso.");
                    $scope.city = city;
                }).error(function (erro) {
                    console.log($scope.city);
                    console.log(erro);
                    pzMessages.messageShow('Não foi possível alterar a cidade ' + $scope.city.name);
                });
            } else {
                $http.post(uriRest + "/city", $scope.city).success(function () {
                    $scope.city = {};
                    pzMessages.messageShow("Cidade adicionada com sucesso");
                }).error(function (erro) {
                    console.log($scope.city);
                    console.log(erro);
                    pzMessages.messageShow("Não foi possível adicionar a cidade");
                });
            }
        }
    };
});