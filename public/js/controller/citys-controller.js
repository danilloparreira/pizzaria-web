/* global uriRest */

angular.module('pizzariaAPP').controller('CitysController', function ($scope, $http, pzMessages, $mdDialog) {
    $scope.citys = [];
    $http.get(uriRest + '/city').success(function (citys) {
        $scope.citys = citys;
    }).error(function (erro) {
        console.log(erro);
    });
    $scope.confirmDeletion = function (ev, city) {
        var confirm = $mdDialog.confirm()
                .title('Excluir cidade?')
                .textContent('Tem certeza que deseja excluir a cidade ' + city.name + '?')
                .ariaLabel('Lucky day')
                .targetEvent(ev)
                .ok('Sim')
                .cancel('Não');
        $mdDialog.show(confirm).then(function () {
            $http.delete(uriRest + '/city/' + city.id).success(function () {
                var indexCity = $scope.citys.indexOf(city);
                $scope.citys.splice(indexCity, 1);
                pzMessages.messageShow('Cidade ' + city.name + ' foi removida com sucesso.');
            }).error(function (erro) {
                console.log(erro);
                pzMessages.messageShow('Não foi possível remover a cidade ' + city.name + '.');
                pzMessages.messageShow('Existe um ou mais setor(es) relacionado com esta cidade.', 4000);
            });
        }, function () {
            pzMessages.messageShow('Cancelado exclusão do estado : ' + city.name);
        });
    };
});

