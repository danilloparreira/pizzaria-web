/* global uriRest */

angular.module('pizzariaAPP').controller('DistrictController', function ($scope, $http, $routeParams, pzMessages) {
    $scope.district = {};
    
    if ($routeParams.id) {
        $http.get(uriRest + '/district/' + $routeParams.id).success(function (district) {
            $scope.district = district;
        }).error(function (erro) {
            console.log(erro);
            pzMessages.messageShow("Não foi possível obter o setor");
        });
    }

    $scope.submit = function () {
        if ($scope.form.$valid) {
            if ($scope.district.id) {
                deleteDate($scope.district);
                $http.put(uriRest + '/district/' + $scope.district.id, $scope.district).success(function (district) {
                    pzMessages.messageShow("O setor " + $scope.district.name + " atualizado com sucesso.");
                    $scope.district = district;
                }).error(function (erro) {
                    console.log(erro);
                    console.log($scope.district);
                    
                    pzMessages.messageShow('Não foi possível alterar o setor ' + $scope.district.name);
                });
            } else {
                $http.post(uriRest + "/district", $scope.district).success(function () {
                    $scope.district = {};
                    pzMessages.messageShow("Estado adicionado com sucesso");
                }).error(function (erro) {
                    console.log(erro);
                    pzMessages.messageShow("Não foi possível adicionar o setor");
                });
            }
        }
    };
});