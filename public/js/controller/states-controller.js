/* global uriRest */

angular.module('pizzariaAPP').controller('StatesController', function ($scope, $http, pzMessages, $mdDialog) {
    $scope.states = [];
    $http.get(uriRest + '/state').success(function (states) {
        $scope.states = states;
    }).error(function (erro) {
        console.log(erro);
    });
    $scope.confirmDeletion = function (ev, state) {
        var confirm = $mdDialog.confirm()
                .title('Excluir estado?')
                .textContent('Tem certeza que deseja excluir o estado ' + state.name + '?')
                .ariaLabel('Lucky day')
                .targetEvent(ev)
                .ok('Sim')
                .cancel('Não');
        $mdDialog.show(confirm).then(function () {
            $http.delete(uriRest + '/state/' + state.id).success(function () {
                var indexState = $scope.states.indexOf(state);
                $scope.states.splice(indexState, 1);
                
                pzMessages.messageShow('Estado ' + state.name + ' foi removido com sucesso.');
            }).error(function (erro) {
                console.log(erro);
                pzMessages.messageShow('Não foi possível remover o estado ' + state.name + '.');
                pzMessages.messageShow('Existe um ou mais cidade(s) relacionado com este estado.', 4000);
            });
        }, function () {
            pzMessages.messageShow('Cancelado exclusão do estado : ' + state.name);
        });
    };
});