/* global uriRest */

angular.module('pizzariaAPP').controller('DistrictsController', function ($scope, $http, pzMessages, $mdDialog) {
    $scope.districts = [];
    $http.get(uriRest + '/district').success(function (districts) {
        $scope.districts = districts;
    }).error(function (erro) {
        console.log(erro);
    });
    $scope.confirmDeletion = function (ev, district) {
        var confirm = $mdDialog.confirm()
                .title('Excluir setor?')
                .textContent('Tem certeza que deseja excluir o setor ' + district.name + '?')
                .ariaLabel('Lucky day')
                .targetEvent(ev)
                .ok('Sim')
                .cancel('Não');
        $mdDialog.show(confirm).then(function () {
            $http.delete(uriRest + '/district/' + district.id).success(function () {
                var indexState = $scope.districts.indexOf(district);
                $scope.districts.splice(indexState, 1);
                
                pzMessages.messageShow('Estado ' + district.name + ' foi removido com sucesso.');
            }).error(function (erro) {
                console.log(erro);
                pzMessages.messageShow('Não foi possível remover o setor ' + district.name + '.');
                pzMessages.messageShow('Existe um ou mais cidade(s) relacionado com este setor.', 4000);
            });
        }, function () {
            pzMessages.messageShow('Cancelado exclusão do setor : ' + district.name);
        });
    };
});