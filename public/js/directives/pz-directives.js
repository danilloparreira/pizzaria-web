var app = angular.module('pzDirectives', []);
app.directive('pzButtonAdd', function(){
    return {
        restrict: 'E',
        scope:{
            href: '@'
        }, template: '<md-button class="md-fab md-warn btn-add" aria-label="Add" ng-href="{{href}}" ><md-icon md-font-set="material-icons"> add </md-icon></md-button>'
    };
}).directive('pzButtonEdit', function () {
    return {
        restrict: 'E',
        scope: {
            href: '@'
        },
        template: '<md-button class="md-fab md-mini md-primary md-button md-ink-ripple" aria-label="Editar" ng-href="{{href}}" ><md-icon md-font-set="material-icons" style="font-size: 15px">create</md-icon></md-button>'
    };
}).directive('pzButtonRemove', function () {
    return {
        restrict: 'E',
        scope: {
            action: '&'
        },
        template: '<md-button class="md-fab md-mini md-primary md-button md-ink-ripple md-warn"  aria-label="Deletar" data-toggle="modal" ng-click="action(event, entity)"><span class="glyphicon glyphicon-trash"></span></md-button>'
    };
}).directive('pzButtonsSave', function () {
    return{
        restrict: 'E',
        template: '<md-button md-whiteframe="3" class="md-raised md-primary" ng-disabled="form.$invalid" type="submit" >Salvar</md-button>'
    };
}).directive('pzButtonsReturn', function () {
    return{
        restrict: 'E',
        scope: {
            href: '@'
        },
        template: '<md-button md-whiteframe="3" ng-href="{{href}}">Voltar</md-button>'
    };
}).directive('pzSection', function () {
    return{
        restrict: 'E',
        transclude: true,
        template: '<section layout="row" layout-sm="column" layout-align="end center" ng-transclude> </section>'
    };
}).directive('pzTitle', function () {
    return{
        restrict: 'E',
        transclude: true,
        template: '<h1 class="col-lg-12" ng-transclude></h1>'
    };
}).directive('pzCard', function(){
    return{
        restrict: 'E',
        transclude: true,
        template: '<md-card flex class="col-md-12" md-whiteframe="2" ng-transclude></md-card>'
    };
}).directive('pzCardForm', function(){
    return{
        restrict: 'E',
        transclude: true,
        template: '<md-card flex class="col-md-12" md-whiteframe="2"><div class="col-md-12" ng-transclued></div></md-card>'
    };
}).directive('pzNewEdit', function(){
    return{
        restrict: 'E',
        scope:{
            id: '='
        },
        template: '<span ng-if="!id">Novo</span><span ng-if="id">Editar</span>'
    };
});