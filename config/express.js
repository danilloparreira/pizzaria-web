var express = require('express');
var app = express();

app.use(express.static('./public'));

//Bibliotecas disponibilizadas
app.use('/lib', express.static('./bower_components'));
module.exports = app;
